defmodule ReflexTest do
  use ExUnit.Case
  doctest Reflex

  alias ReflexTest.Sample.Server

  describe "sample implementation" do
    test "can get its internal state" do
      assert %{posts: %{}} == Server.state()
    end

    test "can reset its internal state to its default version" do
      assert :ok == Server.reset()
      assert %{posts: %{}} == Server.state()
    end
  end
end

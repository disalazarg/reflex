defmodule ReflexTest.Sample do
  defmodule Post do
    @moduledoc "Sample top-level struct"
    use Reflex.Model

    defstruct title: nil, content: nil, slug: nil, comments: %{}
  end

  defmodule Comment do
    @moduledoc "Sample mid-level struct"
    use Reflex.Model

    defstruct content: nil, slug: nil
  end

  defmodule Server do
    @moduledoc "Sample state server"
    use Reflex, state: %{posts: %{}}

    defmodel(Post, [:post])
    defmodel(Comment, [:post, :comment])
  end
end

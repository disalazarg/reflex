defmodule ReflexTest.TopLevelTest do
  use ExUnit.Case

  alias ReflexTest.Sample.{Post, Server}

  describe "when given a top-level model" do
    setup _tags do
      on_exit(fn -> Server.reset() end)

      post = %Post{title: "test-title", content: "some content"}

      {:ok, %{post: post}}
    end

    test "list returns with data", %{post: post} do
      {:ok, post} = Server.put(Post, ["hello-world"], post)

      assert [post] == Server.list(Post, [])
    end

    test "put inserts data on the state", %{post: post} do
      assert {:ok, post} = Server.put(Post, ["hello-world"], post)
      assert {:ok, post} == Server.get(Post, ["hello-world"])
    end

    test "get fails gracefully when given a non-existing path" do
      assert {:error, :not_found} == Server.get(Post, ["hello-world"])
    end
  end
end

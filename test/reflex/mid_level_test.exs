defmodule ReflexTest.MidLevelTest do
  use ExUnit.Case

  alias ReflexTest.Sample.{Comment, Post, Server}

  describe "when given a mid-level model" do
    setup _tags do
      on_exit(fn -> Server.reset() end)

      post = %Post{title: "test-title", content: "some content"}
      comment = %Comment{content: "some content"}

      {:ok, post} = Server.put(Post, [post.slug], post)
      {:ok, %{post: post, comment: comment}}
    end

    test "list returns with data", %{post: post, comment: comment} do
      assert {:ok, comment} = Server.put(Comment, [post.slug, comment.slug], comment)

      assert [comment] == Server.list(Comment, [post.slug])
    end

    test "list fails gracefully when given a non-existing parent" do
      assert [] == Server.list(Comment, ["non-existing"])
    end

    test "put inserts data on the state", %{post: post, comment: comment} do
      assert {:ok, comment} = Server.put(Comment, [post.slug, comment.slug], comment)

      assert {:ok, comment} == Server.get(Comment, [post.slug, comment.slug])
    end

    test "get fails gracefully when given a non-existing path", %{post: post} do
      assert {:error, :not_found} == Server.get(Comment, [post.slug, "non-existing"])
    end

    test "get fails gracefully when given a non-existing parent" do
      assert {:error, :not_found} == Server.get(Comment, ["non-existing", "non-existing"])
    end
  end
end

defmodule Reflex.Base do
  @moduledoc """
  Defines macros for adding the basic structure of a Reflex server
  """

  defmacro __add_basic_structure__(state) do
    quote do
      use GenServer

      @base_state unquote(state)

      def init(opts), do: {:ok, opts}
      def start_link(_state), do: GenServer.start_link(__MODULE__, @base_state, name: __MODULE__)

      # Basic API
      def reset, do: GenServer.call(__MODULE__, {:reset})
      def state, do: GenServer.call(__MODULE__, {:state})

      def handle_call({:reset}, _from, _state) do
        {:reply, :ok, @base_state}
      end

      def handle_call({:state}, _from, state) do
        {:reply, state, state}
      end
    end
  end

  defmacro __add_high_level_api__(_opts) do
    quote do
      def list(model, path), do: GenServer.call(__MODULE__, {:list, model, path})
      def get(model, path), do: GenServer.call(__MODULE__, {:get, model, path})
      def put(model, path, data), do: GenServer.call(__MODULE__, {:put, model, path, data})
      def update(model, path, data), do: GenServer.call(__MODULE__, {:update, model, path, data})
    end
  end
end

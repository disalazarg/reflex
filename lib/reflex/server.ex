defmodule Reflex.Server do
  @moduledoc """
  Defines helpers for use on a Reflex serverx
  """

  alias Reflex.Helpers

  defmacro defmodel(model, path, opts \\ [])

  defmacro defmodel(model, path, _opts) do
    model = Macro.expand(model, __CALLER__)
    args = Enum.map(path, fn elem -> elem |> Macro.var(__MODULE__) end)
    path = Enum.map(path, fn elem -> elem |> Inflex.pluralize() |> String.to_existing_atom() end)
    route = Helpers.routify(path, args)

    quote do
      require Reflex.Macros

      Reflex.Macros.create_list(
        unquote(model),
        unquote(Helpers.del_arg(args)),
        unquote(Helpers.del_arg(route))
      )

      Reflex.Macros.create_get(unquote(model), unquote(args), unquote(route))
      Reflex.Macros.create_put(unquote(model), unquote(args), unquote(route))
      Reflex.Macros.create_update(unquote(model), unquote(args), unquote(route))
    end
  end
end

defmodule Reflex.Model do
  @moduledoc """
  Includes a basic implementation of the Access behaviour for structs
  """

  defmacro __using__(_opts) do
    quote do
      defdelegate fetch(term, key), to: Map
      defdelegate get(term, key, default), to: Map
      defdelegate get_and_update(term, key, fun), to: Map
    end
  end
end

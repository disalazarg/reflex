defmodule Reflex.Helpers do
  @moduledoc """
  Defines helpers for internal use by other Reflex modules
  """

  def routify(path, args, res \\ [])
  def routify([path | patht], [arg | argt], res), do: routify(patht, argt, res ++ [path, arg])
  def routify(path = [_hd | _tl], [], res), do: res ++ path
  def routify([], _args, res), do: res

  def del_arg(list) do
    list
    |> Enum.reverse()
    |> tl()
    |> Enum.reverse()
  end
end

defmodule Reflex.Macros do
  @moduledoc """
  Defines macros to create handlers for high-level API calls
  """

  defmacro create_list(model, args, route) do
    quote do
      def handle_call({:list, unquote(model), unquote(args)}, _from, state) do
        data =
          state
          |> get_in(unquote(route))

        result = Map.values(data || %{})

        {:reply, result, state}
      end
    end
  end

  defmacro create_get(model, args, route) do
    quote do
      def handle_call({:get, unquote(model), unquote(args)}, _from, state) do
        data =
          state
          |> get_in(unquote(route))

        result = if is_nil(data), do: {:error, :not_found}, else: {:ok, data}
        {:reply, result, state}
      end
    end
  end

  defmacro create_put(model, args, route) do
    quote do
      def handle_call({:put, unquote(model), unquote(args), data}, _from, state) do
        state =
          state
          |> put_in(unquote(route), data)

        {:reply, {:ok, data}, state}
      end
    end
  end

  defmacro create_update(model, args, route) do
    quote do
      def handle_call({:update, unquote(model), unquote(args), data}, _from, state) do
        state =
          state
          |> update_in(unquote(route), fn origin ->
            Map.merge(origin, data)
          end)

        {:reply, {:ok, data}, state}
      end
    end
  end
end

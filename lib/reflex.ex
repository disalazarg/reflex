defmodule Reflex do
  @moduledoc """
  Documentation for `Reflex`, a library for defining an API that store tree-based state data.
  """

  defmacro __using__(args) do
    state = Keyword.get(args, :state, Macro.escape(%{}))

    quote do
      import Reflex.Server
      require Reflex.Base

      Reflex.Base.__add_basic_structure__(unquote(state))
      Reflex.Base.__add_high_level_api__(unquote(args))
    end
  end
end
